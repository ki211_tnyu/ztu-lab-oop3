using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int[] nums = new int[3];
            try
            {
                nums[0] = int.Parse(textBox1.Text);
                nums[1] = int.Parse(textBox2.Text);
                nums[2] = int.Parse(textBox3.Text);
                pictureBox1.Visible = false;

                if (nums[0] == nums[1] && nums[0] == nums[2] && nums[1] == nums[2])
                {
                    pictureBox1.Visible = true;
                    pictureBox1.Image = WinFormsApp1.Properties.Resources.Green_Circle2;
                }
                else
                {
                    pictureBox1.Visible = true;
                    pictureBox1.Image = WinFormsApp1.Properties.Resources.Red_Circle1;
                }
            }
            catch
            {
                MessageBox.Show("����� �� ������� ���� ������!!!", "�������", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            }
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }
    }
}