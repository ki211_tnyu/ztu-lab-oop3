﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Label_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void label1_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void label2_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void label3_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void label4_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox2_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox3_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

            private void Табулювання_значення_функції_Loaded(object sender, RoutedEventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            double a, b, h, y, x;
            if (double.TryParse(TextBox1.Text, out a)) { }
            else
            {
                MessageBox.Show("Помилка введення значення a!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (double.TryParse(TextBox2.Text, out b)) { }
            else
            {
                MessageBox.Show("Помилка введення значення b!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (double.TryParse(TextBox3.Text, out h)) { }
            else
            {
                MessageBox.Show("Помилка введення значення h!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            TextBox4.Text = ("x\ty\n");
            for (x = a; x <= b; x += h)
            {
                y = (Math.Log10(Math.Abs(x + 1)) + 5) / (2 * x + 3); ;
                TextBox4.Text += String.Format("{0:F3}\t{1:F3}\n", x, y);
            }
        }
    }
}

       
