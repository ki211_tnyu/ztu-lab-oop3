﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            int[] nums = new int[3];
            try
            {
                nums[0] = int.Parse(TextBox1.Text);
                nums[1] = int.Parse(TextBox2.Text);
                nums[2] = int.Parse(TextBox3.Text);
                Green_Circle.Visibility = Visibility.Collapsed;
                Red_Circle.Visibility = Visibility.Collapsed;

                if (nums[0] == nums[1] && nums[0] == nums[2] && nums[1] == nums[2])
                {
                    Green_Circle.Visibility = Visibility.Visible;
                }
                else
                {
                    Red_Circle.Visibility = Visibility.Visible;
                }
            }
            catch
            {
                MessageBox.Show("Рядок не повинен бути пустим!!!", "Помилка", MessageBoxButton.OK,
                MessageBoxImage.Error);
            }
        }
        private void Image_Loaded(object sender, RoutedEventArgs e)
        {

        }

    }
}

       
