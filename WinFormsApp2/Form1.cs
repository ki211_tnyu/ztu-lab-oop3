using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace WinFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        }
        private void button1_Click(object sender, EventArgs e)
        {
            double a, b, h,x,y;
            if (double.TryParse(textBox1.Text, out a)) { }
            else
            {
                MessageBox.Show("������� �������� �������� a!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (double.TryParse(textBox2.Text, out b)) { }
            else
            {
                MessageBox.Show("������� �������� �������� b!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (double.TryParse(textBox3.Text, out h)) { }
            else
            {
                MessageBox.Show("������� �������� �������� h!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            richTextBox1.Text = ("x\ty\n");
            for (x = a; x <= b; x += h)
            {
                y = (Math.Log10(Math.Abs(x+1))+5)/(2*x+3);
                richTextBox1.Text += String.Format("{0:F3}\t{1:F3}\n", x, y);
            }
            }
    }
}